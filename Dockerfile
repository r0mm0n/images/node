FROM docker.io/node:20.14.0-alpine3.19

RUN apk add --update --no-cache bash zsh yarn git git-lfs python3 icu-data-full
